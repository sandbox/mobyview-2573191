<?php

/**
 * @file
 * Mobyview module API file.
 */

/**
 * Serialize a custom entity.
 *
 * This method is called when a entity is rendered by service.
 *
 * @param array &$data
 *   Current entity serialized.
 * @param array $context
 *   Context (name, property (wrapper), parent, ctrl, fields).
 */
function hook_mobyview_entity_service_get_data_alter(array &$data, array $context) {
  /** @var \MobyviewEntityServicesResourceControllerClean $ctrl */
  $ctrl     = $context['ctrl'];
  $name     = $context['name'];
  $parent   = $context['parent'];
  $property = $context['property'];

  $type = $property->type();

  $data[$name]['formated'] = 'xxxx';

  if ($name == 'commerce_product') {
    $data[$name] = $ctrl->format(entity_metadata_wrapper('commerce_product', $property));
  }
}
