<?php

/**
 * @file
 * Class util for serialize and unserialize file field.
 */

/**
 * Class MobyviewEntityFileUtil.
 */
class MobyviewEntityFileUtil {
  /**
   * Extract previews from request.
   *
   * @return array
   *   Previews.
   */
  public function extractPreviews() {
    $styles_array = array();

    if (isset($_GET['extra_previews'])) {
      $styles_array = explode(',', t($_GET['extra_previews']));
    }

    return $styles_array;
  }

  /**
   * Serialize file object json.
   *
   * @param object $file
   *   File.
   * @param array|null $previews
   *   Previews.
   *
   * @return array
   *   File formatted.
   */
  public function formatFile($file, $previews = NULL) {
    $data = array();

    if (isset($file) && isset($file->uri)) {
      $data = $this->getResourceReference('file', $file->fid);
      $data['uri_full'] = file_create_url($file->uri);
      $data['previews'] = $this->makePreviews($file->uri, ($previews == NULL ? $this->extractPreviews() : $previews));
    }

    return $data;
  }

  /**
   * Format resource.
   *
   * @param string $resource_type
   *   Resource type.
   * @param mixed $id
   *   Reference id.
   *
   * @return array
   *   Resource.
   */
  protected function getResourceReference($resource_type, $id) {
    $return = array(
      'uri' => services_resource_uri(array('entity_' . $resource_type, $id)),
      'id' => $id,
      'resource' => $resource_type,
    );

    if (module_exists('uuid') && entity_get_info($resource_type)) {
      $ids = entity_get_uuid_by_id($resource_type, array($id));

      if ($id = reset($ids)) {
        $return['uuid'] = $id;
      }
    }

    return $return;
  }

  /**
   * Create path url for one preview file.
   *
   * @param array $style
   *   Image style.
   * @param string $uri
   *   File uri.
   *
   * @return mixed
   *   Link.
   */
  protected function makeLinkStyle(array $style, $uri) {
    $derivative_uri = image_style_path($style['name'], $uri);
    file_exists($derivative_uri) || image_style_create_derivative($style, $uri, $derivative_uri);

    return file_create_url($derivative_uri);
  }

  /**
   * Make previews image if extra_previews.
   *
   * @param string $uri
   *   File uri.
   * @param array $styles_array
   *   Styles.
   *
   * @return array
   *   Image preview.
   */
  protected function makePreviews($uri, array $styles_array) {
    $styles = image_styles();
    $arr = array();

    foreach ($styles as $style) {
      if (in_array($style['name'], $styles_array)) {
        $arr['uri_style_' . $style['name']] = $this->makeLinkStyle($style, $uri);
      }
    }

    return $arr;
  }

}
