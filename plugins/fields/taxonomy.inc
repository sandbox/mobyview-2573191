<?php

/**
 * @file
 * Class util for serialize and unserialize term.
 */

/**
 * Class MobyviewEntityTaxonomyUtil.
 */
class MobyviewEntityTaxonomyUtil {
  /**
   * File.
   *
   * @var \MobyviewEntityFileUtil
   */
  protected $fileUtil;

  /**
   * MobyviewEntityTaxonomyUtil constructor.
   */
  public function __construct() {
    $this->fileUtil = new MobyviewEntityFileUtil();
  }

  /**
   * Parse taxonomy term.
   *
   * @param object $term
   *   Term.
   * @param string $langcode
   *   Language code.
   *
   * @return array|null
   *   Term transformed.
   */
  public function formatTerm($term, $langcode) {
    if (!isset($term)) {
      return NULL;
    }

    $results = array();

    foreach ($term as $key => $field) {
      $info = field_info_field($key);

      // Fix to include image in taxonomy_term.
      if ($info['type'] == 'image') {
        if (isset($field[LANGUAGE_NONE][0]['fid'])) {
          if ($file = file_load($field[LANGUAGE_NONE][0]['fid'])) {
            $results[$key] = $this->fileUtil->formatFile($file);
          }
        }
      }
      elseif (module_exists('i18n_taxonomy') && $key == 'name') {
        $results[$key] = i18n_taxonomy_term_name($term, $langcode);
      }
      else {
        $results[$key] = $field;
      }
    }

    return $results;
  }

  /**
   * Terms to transform.
   *
   * @param mixed $value
   *   Terms.
   *
   * @return array|null
   *   Terms transformed.
   */
  public function transform($value) {
    if (!is_array($value)) {
      return (empty($value) ? NULL : $value);
    }
    else {
      $arr = array();

      foreach ($value as $v) {
        array_push($arr, (empty($v) ? NULL : $v));
      }

      return $arr;
    }
  }

}
