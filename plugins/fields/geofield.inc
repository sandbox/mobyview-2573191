<?php

/**
 * @file
 * Class util for serialize and unserialize geofield.
 */

/**
 * Class MobyviewEntityGeofieldUtil.
 */
class MobyviewEntityGeofieldUtil {
  /**
   * Parse geofield.
   *
   * @param string $value
   *   Latitude/Longitude.
   *
   * @return mixed
   *   Field.
   */
  public function parseGeofield($value) {
    // Remove value if key is empty.
    if (!empty($value)) {
      // Helper if received.
      $latlong = explode(',', $value);

      if (is_array($latlong) && count($latlong) == 2) {
        $lat = trim($latlong[0]);
        $lon = trim($latlong[1]);
        geophp_load();

        return geofield_get_values_from_geometry(new Point($lon, $lat));
      }
    }

    return NULL;
  }

  /**
   * Parse multiple geofields.
   *
   * @param mixed $value
   *   Geofield(s).
   *
   * @return array|null
   *   Geofield(s) transformed.
   */
  public function transform($value) {
    if (!is_array($value)) {
      return $this->parseGeofield($value);
    }
    else {
      $arr = array();

      foreach ($value as $v) {
        array_push($arr, $this->parseGeofield($v));
      }

      return $arr;
    }
  }

}
