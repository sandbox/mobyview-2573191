<?php

/**
 * @file
 * Class util for serialize and unserialize datefield.
 */

/**
 * Class MobyviewEntityDateUtil.
 */
class MobyviewEntityDateUtil {
  /**
   * Convert date to ISO.
   *
   * @param mixed $date
   *   Date to convert.
   *
   * @return string
   *   Date with ISO format.
   */
  public function formatDateToIso($date) {
    if (!is_null($date)) {
      return format_date($date, 'custom', 'c');
    }
    return NULL;
  }

  /**
   * Convert ISO to unixtimestamp.
   *
   * @param string $iso
   *   Date to convert.
   *
   * @return int
   *   Timestamp.
   */
  public function parseIsoToU($iso) {
    return strtotime($iso);
  }

  /**
   * Convert a date or a pool of dates.
   *
   * @param mixed $value
   *   Date(s).
   *
   * @return array|int
   *   Date(s) converted.
   */
  public function transform($value) {
    if (!is_array($value)) {
      return $this->parseIsoToU($value);
    }
    else {
      $arr = array();

      foreach ($value as $v) {
        array_push($arr, $this->parseIsoToU($v));
      }

      return $arr;
    }
  }

}
