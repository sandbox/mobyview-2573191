<?php

/**
 * @file
 * This class is designed to create a very clean API.
 *
 * We want to strip all "drupalisms" out of the Services API.
 * For example, there should be no [LANGUAGE_NONE][0][value] or
 * field_* in the API.
 *
 * It should be possible to create an API that is easily replicated on another
 * system.
 *
 * Much of this code is borrowed from restws module.
 */

/**
 * Class MobyviewEntityServicesResourceControllerClean.
 */
class MobyviewEntityServicesResourceControllerClean extends ServicesEntityResourceControllerClean {
  /**
   * File helper.
   *
   * @var \MobyviewEntityFileUtil
   */
  protected $fileUtil;

  /**
   * Taxonomy term helper.
   *
   * @var \MobyviewEntityTaxonomyUtil
   */
  protected $taxonomyUtil;

  /**
   * Date helper.
   *
   * @var \MobyviewEntityDateUtil
   */
  protected $dateUtil;

  /**
   * Geofield helper.
   *
   * @var \MobyviewEntityGeofieldUtil
   */
  protected $geofieldUtil;

  /**
   * MobyviewEntityServicesResourceControllerClean constructor.
   */
  public function __construct() {
    $this->fileUtil     = new MobyviewEntityFileUtil();
    $this->taxonomyUtil = new MobyviewEntityTaxonomyUtil();
    $this->dateUtil     = new MobyviewEntityDateUtil();
    $this->geofieldUtil = new MobyviewEntityGeofieldUtil();
  }

  /**
   * Callback when index entities.
   *
   * We override this method just for return an array when there is not result.
   *
   * @param string $entity_type
   *   Filter by entity.
   * @param mixed $fields
   *   Fields to return (for all : '*').
   * @param array $parameters
   *   Filters ex: parameters[field].
   * @param int $page
   *   Current page (offset).
   * @param int $pagesize
   *   Max result.
   * @param array $sort
   *   Fields to sort.
   * @param string $direction
   *   Direction ASC/DESC.
   *
   * @return array
   *   Entities.
   *
   * @throws \Exception
   *   Unsupported error thrown to the upper level.
   */
  public function index(
    $entity_type,
    $fields,
    $parameters,
    $page,
    $pagesize,
    $sort,
    $direction
  ) {
    try {
      return parent::index(
        $entity_type,
        $fields,
        $parameters,
        $page,
        $pagesize,
        $sort,
        $direction
      );
    }
    catch (ServicesException $e) {
      $error_code = $e->getCode();
      $error_message = $e->getMessage();

      // Return array empty if no result.
      if ($error_code == '404' && $error_message == 'No entities found.') {
        return array();
      }
      else {
        throw $e;
      }
    }
  }

  /**
   * Override create method for return the current entity created.
   *
   * @param string $entity_type
   *   Entity type.
   * @param array $values
   *   Fields.
   *
   * @return array
   *   Data.
   *
   * @throws \ServicesException
   *   Unknown data properties.
   */
  public function create($entity_type, array $values) {
    $wrapper = $this->createWrapperFromValues($entity_type, $values);

    // Check write access on each property.
    foreach (array_keys($values) as $name) {
      if (!$this->propertyAccess($wrapper, $name, 'create')) {
        services_error(
          t("Not authorized to set property '@p'", array('@p' => $name)),
          403
        );
      }
    }

    // Make sure that bundle information is present on entities that have
    // bundles. We have to do this after creating the wrapper, because the
    // name of the bundle key may differ from that of the corresponding
    // metadata property (e.g. for taxonomy terms, the bundle key is
    // 'vocabulary_machine_name', while the property is 'vocabulary').
    if ($bundle_key = $wrapper->entityKey('bundle')) {
      $entity = $wrapper->value();

      if (empty($entity->{$bundle_key})) {
        $entity_info = $wrapper->entityInfo();

        if (isset($entity_info['bundles']) && count(
            $entity_info['bundles']
          ) === 1
        ) {
          // If the entity supports only a single bundle, then use that as a
          // default. This allows creation of such entities if (as with ECK)
          // they still use a bundle key.
          $entity->{$bundle_key} = reset($entity_info['bundles']);
        }
        else {
          services_error('Missing bundle: ' . $bundle_key, 406);
        }
      }
    }

    $properties = $wrapper->getPropertyInfo();
    $diff       = array_diff_key($values, $properties);

    if (!empty($diff)) {
      services_error(
        'Unknown data properties: ' . implode(' ', array_keys($diff)) . '.',
        406
      );
    }

    try {
      $entity = $wrapper->value();
      // Mobyview: add validate fields.
      field_attach_validate($entity_type, $entity);
    }
    catch (FieldValidationException $e) {
      // Re-throw this as a ServicesException.
      $field_errors = $e->errors;
      services_error(
        t(
          'Invalid data for entity in the following fields: @fieldnames.',
          array(
            '@fieldnames' => implode(', ', array_keys($field_errors)),
          )
        ),
        400
      );
    }

    $wrapper->save();

    // Mobyview: we have override the create method because
    // we had to serialize wrapper with get_data.
    return $this->getData($wrapper, '*');
  }

  /**
   * Format wrapper.
   *
   * @param \EntityMetadataWrapper $wrapper
   *   Wrapper.
   * @param string $fields
   *   Fields.
   * @param mixed $parent
   *   Parent.
   *
   * @return array
   *   Wrapper formatted.
   */
  public function format(\EntityMetadataWrapper $wrapper, $fields = '*', $parent = NULL) {
    return $this->getData($wrapper, $fields, $parent);
  }

  /**
   * Override Serialize an entity.
   *
   * @param \EntityMetadataWrapper $wrapper
   *   Wrapper.
   * @param string $fields
   *   Fields.
   *
   * @return array
   *   Wrapper formatted.
   */
  protected function get_data($wrapper, $fields = '*') {
    return $this->getData($wrapper, $fields);
  }

  /**
   * Override serialize wrapper.
   *
   * Explains:
   * - in queryString,
   * - exclude : exlude fields
   * - include_node : include all fields for a relationship entity
   *      (separate fields by ',')
   * - include_fields : select fields to return
   *      (separate fields ',' and ';' for other node)
   * - language: select fields localized
   * - extra_author: public information on the entity author
   *      (by default: * or fields separated by comma)
   * - extra_previews: media style (separate style by comma)
   *
   * @param \EntityMetadataWrapper $wrapper
   *   Wrapper.
   * @param string $fields
   *   Fields to return (ex: field1, field2).
   * @param mixed $parent
   *   Parent.
   *
   * @return array
   *   Data.
   */
  private function getData(\EntityMetadataWrapper $wrapper, $fields = '*', $parent = NULL) {
    watchdog('mobyview', '[get_data] fields:' . $wrapper->type());
    if ($fields != '*') {
      $fields_array = explode(',', $fields);
    }
    else {
      $fields_array = array();
    }

    $exclude_fields = array();
    if (isset($_GET['exclude'])) {
      $exclude_fields = explode(',', t($_GET['exclude']));
    }

    $include_node = array();
    if (isset($_GET['include_node'])) {
      $include_node = explode(',', t($_GET['include_node']));
    }

    $include_node_fields = array();
    if (isset($_GET['include_node_fields'])) {
      $include_node_fields = explode(';', t($_GET['include_node_fields']));
    }

    $data = array();

    // Handle language.
    $language = LANGUAGE_NONE;

    if (isset($_GET['language']) && !empty($_GET['language'])) {
      if ($wrapper instanceof EntityDrupalWrapper || $wrapper instanceof EntityStructureWrapper) {
        $language = t($_GET['language']);

        if (strpos($language, '_')) {
          $language = strstr($language, '_', TRUE);
        }
        elseif (strpos($language, '-')) {
          $language = strstr($language, '-', TRUE);
        }
        $wrapper = $wrapper->language($language);
      }
    }

    $filtered = $this->property_access_filter($wrapper);

    // Hack to return picture.
    if ($wrapper->type() == 'user') {
      $u = user_load($wrapper->getIdentifier());
      if (isset($u)) {
        // Add picture user.
        $data['picture'] = $this->fileUtil->formatFile($u->picture);
      }
    }

    foreach ($filtered as $name => $property) {
      // We don't want 'field_' at the beginning of fields.
      // This is a drupalism and shouldn't be in the api.
      $name = preg_replace('/^field_/', '', $name);

      // Code: "watchdog('mobyview', '[get_data] name:' . $name);".
      // If fields is set and it isn't one of them, go to the next.
      if ($fields != '*' && !in_array($name, $fields_array) || in_array(
          $name,
          $exclude_fields
        )
      ) {
        continue;
      }

      try {
        if ($property instanceof EntityDrupalWrapper) {
          // For referenced entities only return the URI.
          if ($id = $property->getIdentifier()) {

            $ref = $this->get_resource_reference($property->type(), $id);

            if ($property->type() == 'file') {
              // Fix : to include file.
              if ($file = file_load($id)) {
                $data[$name] = $this->fileUtil->formatFile($file);
              }
              else {
                $data[$name] = $ref;
              }
            }
            elseif ($property->type() == 'taxonomy_term') {
              // Include taxonomy_term.
              $term = taxonomy_term_load($id);

              if ($term !== FALSE) {
                $data[$name] = $this->taxonomyUtil->formatTerm(
                  $term,
                  $language
                );
              }
            }
            elseif ($property->type() == 'user') {
              // Add user info IF extra_author.
              if (isset($_GET['extra_author'])) {

                $extra_author_value = t($_GET['extra_author']);

                if (empty($extra_author_value)) {
                  $extra_author_value = '*';
                }
                $data[$name] = $this->getData($property, $extra_author_value);

                $fields_author = array();
                if ($extra_author_value != '*') {
                  $fields_author = explode(',', $extra_author_value);
                }

                if ($extra_author_value == '*' || in_array(
                    'picture',
                    $fields_author
                  )
                ) {
                  $u = user_load($id);
                  $data[$name]['picture'] = $this->fileUtil->formatFile($u->picture);
                }
              }
            }
            elseif ($property->type() == 'node') {
              // Check if include_node='fields'.
              $data[$name] = $this->includeFields(
                $property,
                $name,
                $parent,
                $ref,
                $include_node,
                $include_node_fields
              );
            }
            else {
              // Check if include_node='fields'.
              $data[$name] = $this->includeFields(
                $property,
                $name,
                $parent,
                $ref,
                $include_node,
                $include_node_fields
              );
            }
          }
        }
        elseif ($property instanceof EntityValueWrapper) {
          if ($property->type() == 'date') {
            // Format date to iso 8601.
            $data[$name] = $this->dateUtil->formatDateToIso($property->value());
          }
          else {
            $data[$name] = $property->value();
          }
        }
        elseif ($property instanceof EntityListWrapper || $property instanceof EntityStructureWrapper) {

          if ($property->type() == 'geofield') {
            // Serialize latlon.
            $data[$name] = $this->getData($property, 'latlon');
          }
          else {
            $data[$name] = $this->getData(
              $property,
              '*',
              $name
            // fix: add name : for array.
            );
          }
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // A property causes problems - ignore that.
        // watchdog('mobyview_entity', 'error : ' . $name . ' --> ' . $e);.
      }

      // HOOK.
      $context = array(
        'name' => $name,
        'property' => $property,
        'ctrl' => $this,
        'parent' => $parent,
      );
      drupal_alter('mobyview_entity_service_get_data', $data, $context);
    }

    // HOOK.
    $context = array(
      'name' => NULL,
      'property' => $wrapper,
      'ctrl' => $this,
      'parent' => $parent,
    );
    drupal_alter('mobyview_entity_service_get_data', $data, $context);

    // If bundle = entity_type, don't send it.
    if (method_exists($wrapper, 'entityInfo')) {
      $entity_info = $wrapper->entityInfo();

      if (isset($entity_info['bundle keys'])) {
        foreach ($entity_info['bundle keys'] as $bundle_key) {
          if (array_key_exists($bundle_key, $data) &&
            $data[$bundle_key] == $wrapper->type()) {
            unset($data[$bundle_key]);
          }
        }
      }
    }

    return $data;
  }

  /**
   * Include and normalize fields.
   *
   * @param mixed $property
   *   Property.
   * @param string $name
   *   Name.
   * @param string $parent
   *   Parent.
   * @param mixed $ref
   *   Ref.
   * @param array $include_node
   *   Include node.
   * @param array $include_node_fields
   *   Include node fields.
   *
   * @return mixed
   *   Result.
   */
  protected function includeFields(
    $property,
    $name,
    $parent,
    $ref,
    $include_node,
    $include_node_fields
  ) {
    // Check if include_node='fields'.
    $checkfields = $name;

    if ((is_numeric($checkfields) && $parent != NULL)) {
      // Is array {.
      $checkfields = $parent;
    }

    if (FALSE !== ($index = array_search($checkfields, $include_node))) {
      return $this->getData(
        $property,
        isset($include_node_fields[$index]) ? $include_node_fields[$index] : '*'
      );
    }
    else {
      return $ref;
    }
  }

  /**
   * Transform values using custom logic.
   *
   * @inheritdoc
   */
  protected function transform_values($entity_type, $property_info, $values) {
    watchdog('mobyview', 'transform_values');

    $values = parent::transform_values($entity_type, $property_info, $values);
    $arr    = NULL;

    foreach ($values as $key => &$value) {
      if (isset($property_info[$key]) && isset($property_info[$key]['type'])) {
        $type = $property_info[$key]['type'];

        // If multi values.
        if (drupal_substr($type, 0, 4) === 'list') {
          // If no array.
          if (!is_array($value)) {
            // Create array.
            $arr = array();
            array_push($arr, $value);
            $value = $arr;
          }
        }
        else {
          // IF SINGLE VALUE AND IS ARRAY with key number.
          if (is_array($value) && !(array_keys($value) !== range(
                0,
                count($value) - 1
              ))
          ) {
            $value = $value[0];
          }
        }

        if ($type == 'node' || $type == 'list<node>') {
          // debug($value);
          if (!is_array($value)) {
            $values[$key] = (empty($value) ? NULL : $value);
          }
          else {
            $arr = array();

            foreach ($value as $v) {
              array_push($arr, (empty($v) ? NULL : $v));
            }

            $values[$key] = $arr;
          }
        }

        if ($type == 'taxonomy_term' || $type == 'list<taxonomy_term>') {
          $values[$key] = $this->taxonomyUtil->transform($value);
        }

        if ($type == 'date' || $type == 'list<date>') {
          $values[$key] = $this->dateUtil->transform($value);
        }
        elseif ($type == 'geofield' || $type == 'list<geofield>') {
          // lat,long.
          $values[$key] = $this->geofieldUtil->transform($value);
        }
        elseif ($type == 'field_item_image') {
          // Helper : set NULL if fid == 0.
          if (is_array($value) && count($value) == 1 &&
            isset($value['fid']) && $value['fid'] == 0
          ) {
            $values[$key] = NULL;
          }
        }
      }

      unset($value);
    }

    return $values;
  }

  /**
   * Override.
   *
   * @param \EntityMetadataWrapper $wrapper
   *   Wrapper.
   * @param string $name
   *   Name.
   * @param string $op
   *   Operation.
   *
   * @return bool
   *   Access.
   */
  protected function propertyAccess($wrapper, $name, $op) {
    global $user;

    switch ($op) {
      case 'update':
        // Fix update language. by default, user must have
        // the permission 'administer user'.
        if ($name == 'language' && $wrapper->type() == 'user') {
          if ($wrapper->value()->uid == $user->uid) {
            return TRUE;
          }
        }
    }

    return parent::propertyAccess($wrapper, $name, $op);
  }

}
